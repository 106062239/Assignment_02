var menuState = {
    create: function() {
        // Add a background image
        game.add.image(0, 0, 'background');
        var startLabel = game.add.text(500, game.height-80,
        'Play', { font: '25px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);
        startLabel.inputEnabled = true;
        startLabel.events.onInputDown.add(this.start, this);
        startLabel.events.onInputOver.add(this.over, this);
        startLabel.events.onInputOut.add(this.out, this);
        var startLabel_online = game.add.text(100, game.height-80,
            'Play Online', { font: '25px Arial', fill: '#ffffff' });
            startLabel_online.anchor.setTo(0.5, 0.5);
            startLabel_online.inputEnabled = true;
            startLabel_online.events.onInputDown.add(this.start_online, this);
            startLabel_online.events.onInputOver.add(this.over, this);
            startLabel_online.events.onInputOut.add(this.out, this);
    },
    over: function(item) {
        item.setStyle({fill: 'skyblue'});
    },
    out: function(item) {
        item.setStyle({ font: '25px Arial' ,fill: '#fff'});
    },
    start: function() {
        // Start the actual game
        game.global.playerName = prompt('Please enter your name', 'Your Name');
        game.state.start('play');
    },
    start_online: function(){
        window.location.href = "https://assignment02-106062239.herokuapp.com/";
    }
};