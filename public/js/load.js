var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,
        'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        // Load all game assets
        game.load.spritesheet('player', 'assets/player.png', 36, 42);
        game.load.spritesheet('player2', 'assets/player2.png', 50, 33);
        game.load.image('enemy', 'assets/enemy.png');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('bullet2', 'assets/bullet2.png');
        game.load.image('superbullet', 'assets/super_bullet.png');
        game.load.image('enemybullet', 'assets/enemy_bullet.png');
        game.load.image('powerbullet', 'assets/power_bullet.png');
        game.load.image('flame', 'assets/flame.png');
        game.load.image('tree', 'assets/tree.png');
        // Load a new asset that we will use in the menu state
        game.load.image('background', 'assets/background.png');
        game.load.image('game_background', 'assets/bg.png');
        game.load.image('gameover', 'assets/gameover.png');
        game.load.audio('bgm', ['sound/RaidenBGM.ogg', 'sound/RaidenBGM.mp3']);
        game.load.audio('shootSound', ['sound/LaserGun.ogg', 'sound/LaserGun.mp3']);
        // Load game UI
        game.load.image('sound_icon', 'assets/sound_icon.png');
        game.load.image('mute', 'assets/mute.png');
        game.load.image('plus', 'assets/plus.png');
        game.load.image('minus', 'assets/minus.png');
        game.load.image('pause', 'assets/pause.png');
        game.load.image('resume', 'assets/resume.png');
        game.load.image('blood', 'assets/blood.png');
        game.load.image('power_icon', 'assets/power_icon.png');
        // buff
        game.load.image('powerbullet_icon', 'assets/powerbullet_icon.png');
        game.load.image('defend_icon', 'assets/defend_icon.png');
        game.load.image('aiming_icon', 'assets/aiming_icon.png');
        game.load.image('save_circle', 'assets/save_circle.png');
        // boss
        game.load.image('boss', 'assets/boss.png');
        game.load.image('bossbullet', 'assets/boss_bullet.png');
        // warning
        game.load.image('warning', 'assets/warning.png');
    },
    create: function() {
        // Go to the menu state
        game.global.bgm = game.add.audio('bgm',0.7,true);
        game.global.bgm.play();
        game.state.start('menu');
    }
}; 