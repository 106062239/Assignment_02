var overState = {
    create: function() {
        // Add a background image
        game.add.image(0, -100, 'gameover');
        game.global.again = true;
        // leaderboard
        var scoretext = 'Rank        Name        Score'
        var scoreLabel = game.add.text(game.width/2, game.height/2,
            scoretext, { font: '25px Geo', fill: '#ffffff', align: 'center' });
            scoreLabel.anchor.setTo(0.5, 0.5);
            var updateRef = firebase.database().ref('Record/' + game.global.playerName);
            updateRef.once('value').then(function(snapshot){
                if(snapshot.val() < game.global.score){
                    updateRef.set(game.global.score);
                }
            });
            var leaderboardRef = firebase.database().ref('leaderboard');
            var leaderboardRef_1 = firebase.database().ref('leaderboard/1');
            var leaderboardRef_2 = firebase.database().ref('leaderboard/2');
            var leaderboardRef_3 = firebase.database().ref('leaderboard/3');
            var leaderboardRef_4 = firebase.database().ref('leaderboard/4');
            var leaderboardRef_5 = firebase.database().ref('leaderboard/5');
            var leader_name = [];
            var leader_score = [];
            var textLabel = [];
            leaderboardRef_1.once('value').then(function(s1){
                if(s1.val()!=null){
                    leader_name.push(s1.val().name);
                    leader_score.push(s1.val().score);
                }
                leaderboardRef_2.once('value').then(function(s2){
                    if(s2.val()!=null){
                        leader_name.push(s2.val().name);
                        leader_score.push(s2.val().score);
                    }
                    leaderboardRef_3.once('value').then(function(s3){
                        if(s3.val()!=null){
                            leader_name.push(s3.val().name);
                            leader_score.push(s3.val().score);
                        }
                        leaderboardRef_4.once('value').then(function(s4){
                            if(s4.val()!=null){
                                leader_name.push(s4.val().name);
                                leader_score.push(s4.val().score);
                            }
                            leaderboardRef_5.once('value').then(function(s5){
                                var final_name = [];
                                var final_score = [];
                                var hasIn = false;
                                if(s5.val() != null){
                                    leader_name.push(s5.val().name);
                                    leader_score.push(s5.val().score);
                                }
                                if(leader_name.length==0){
                                    leaderboardRef_1.set({
                                        name: game.global.playerName,
                                        score: game.global.score
                                    }).then(function(){
                                        var text = 1 + ':    ' + game.global.playerName + "    " + game.global.score;
                                        textLabel[0] = game.add.text(game.width/2 - 10, game.height/2 + 30,
                                            text, { font: '25px Geo', fill: '#ffffff'});
                                        textLabel[0].anchor.setTo(0.5,0.5);
                                    });
                                }
                                else{
                                    for(i=0 ; i<5 ; i++){
                                        if(leader_score[i]){
                                            if(game.global.score > leader_score[i]){
                                                final_name.push(game.global.playerName);
                                                final_score.push(game.global.score);
                                                for(j=i; j<5; j++){
                                                    if(j >= leader_name.length) break;
                                                    final_name.push(leader_name[j]);
                                                    final_score.push(leader_score[j]);
                                                }
                                                break;
                                            }
                                            else{
                                                final_name.push(leader_name[i]);
                                                final_score.push(leader_score[i]);
                                            }
                                        }
                                        else{
                                            if(!hasIn){
                                                final_name.push(game.global.playerName);
                                                final_score.push(game.global.score);
                                            }
                                            break;
                                        }
                                    }
                                    for(i=0; i<final_name.length; i++){
                                        if(i >= 5) break;
                                        var setRef = firebase.database().ref('leaderboard/' + Number(i+1));
                                        setRef.set({
                                            name: final_name[i],
                                            score: final_score[i]
                                        });
                                        var text = Number(i+1) + ':    ' + final_name[i] + "    " + final_score[i];
                                        textLabel[i] = game.add.text(game.width/2 - 10, game.height/2 + (i+1)*30,
                                            text, { font: '25px Geo', fill: '#ffffff'});
                                        textLabel[i].anchor.setTo(0.5,0.5);
                                    }
                                }
                            });
                        });
                    });
                });
                leaderboardRef.on('child_changed', function(data){
                    textLabel[data.key-1].setText(data.key+ ':    ' + data.val().name+"    " + data.val().score);
                });
            });
       
        

        var startLabel = game.add.text(450, 500,
            'Play Again', { font: '25px Arial', fill: '#ffffff' })
        startLabel.anchor.setTo(0.5, 0.5);
        startLabel.inputEnabled = true;
        startLabel.events.onInputDown.add(this.start, this);
        startLabel.events.onInputOver.add(this.over, this);
        startLabel.events.onInputOut.add(this.out, this);
        var quitLabel = game.add.text(150, 500,
        'Quit', { font: '25px Arial', fill: '#ffffff' });
        quitLabel.anchor.setTo(0.5, 0.5);
        quitLabel.inputEnabled = true;
        quitLabel.events.onInputDown.add(this.quit, this);
        quitLabel.events.onInputOver.add(this.over, this);
        quitLabel.events.onInputOut.add(this.out, this);
    },
    over: function(item) {
        item.setStyle({fill: 'wheat'});
    },
    out: function(item) {
        item.setStyle({ font: '25px Arial' ,fill: '#fff'});
    },
    start: function() {
        // Start the actual game
        game.state.start('play');
    },
    quit: function() {
        game.state.start('menu');
    }
};