var bootState = {
    init: function(){
        game.stage.disableVisibilityChange = true;
    },
    preload: function () {
        // Load the progress bar image.
        game.load.image('progressBar', 'assets/progressBar.png');
    },
    create: function() {
        // If the device is not a desktop (so it's a mobile device)
        if (!game.device.desktop) {
            // Set the type of scaling to 'show all'
            game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            // Set the min and max width/height of the game
            game.scale.setMinMax(game.width/2, game.height/2,
            game.width*2, game.height*2);
            // Center the game on the screen
            game.scale.pageAlignHorizontally = true;
            game.scale.pageAlignVertically = true;
            // Add a blue color to the page to hide white borders
            document.body.style.backgroundColor = '#3498db';
        }
        // Set some game settings.
        game.stage.backgroundColor = '#262527';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        game.global.again = false;
        // Start the load state.
        game.state.start('load');
    }
};