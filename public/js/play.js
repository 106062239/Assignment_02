var playState = {
    preload: function() {}, // The proload state does nothing now.
    create: function() {
        // background
        var bg = game.add.tileSprite(0, 0, game.width, game.height, 'game_background',1);
        bg.autoScroll(0,50);
        // sound
        this.shootSound = game.add.audio('shootSound');
        this.sound_icon = game.add.button(450,8,'sound_icon');
        this.sound_mute = game.add.button(450,8,'mute');
        this.sound_mute.visible = false;
        this.initClickUI(this.sound_icon);
        this.initClickUI(this.sound_mute);
        this.sound_mute.events.onInputUp.add(this.changeMute, this);
        this.sound_icon.events.onInputUp.add(this.changeMute, this);
        var plus = game.add.button(480,10,'plus');
        this.initClickUI(plus);
        plus.events.onInputDown.add(this.sound_plus, this);
        var minus = game.add.button(510,10,'minus');
        this.initClickUI(minus);
        minus.events.onInputDown.add(this.sound_minus, this);
        game.global.soundSize = 1;
        // UI
        this.pause = game.add.button(420, 10, 'pause');
        this.resume = game.add.button(420, 10, 'resume');
        this.resume.alpha = 0;
        this.pause.inputEnable = true;
        this.resume.inputEnable = true;
        this.pause.events.onInputUp.add(this.changePause, this);
        this.resume.events.onInputUp.add(this.changePause, this);
        // blood
        this.blood_1 = game.add.sprite(510, 530, 'blood');
        this.blood_2 = game.add.sprite(530, 530, 'blood');
        this.blood_3 = game.add.sprite(550, 530, 'blood');
        game.global.health = 3;
        this.blood_1_2 = game.add.sprite(50, 530, 'blood');
        this.blood_2_2 = game.add.sprite(70, 530, 'blood');
        this.blood_3_2 = game.add.sprite(90, 530, 'blood');
        game.global.health_2 = 3;
        // super power state
        this.power_icon = game.add.sprite(470, 530, 'power_icon');
        game.global.powerEnough = true;
        game.global.powerState = 0;
        this.power_icon_2 = game.add.sprite(130, 530, 'power_icon');
        game.global.powerEnough_2 = true;
        game.global.powerState_2 = 0;
        // Create player object
        this.player = game.add.sprite(game.width/2,game.height/2, 'player');
        this.player.animations.add('leftwalk', [1], 8, true);
        this.player.animations.add('rightwalk', [3], 8, true);
        this.player.anchor.setTo(0.5, 0);
        game.global.playerDie = false;
        game.global.playerHurt = false;
        // Create player2 object
        this.player2 = game.add.sprite(game.width/2 + 50,game.height/2, 'player2');
        this.player2.animations.add('leftwalk2', [1], 8, true);
        this.player2.animations.add('rightwalk2', [2], 8, true);
        this.player2.anchor.setTo(0.5, 0);
        game.global.player2Die = false;
        game.global.player2Hurt = false;
        // Create enemy
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(6, 'enemy');
        game.time.events.loop(1000, this.addEnemy, this);
        // Create bullet
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple(20, 'bullet');
        game.time.events.loop(500, this.addBullet, this);
        this.bullets2 = game.add.group();
        this.bullets2.enableBody = true;
        this.bullets2.createMultiple(20, 'bullet2');
        game.time.events.loop(500, this.addBullet2, this);
        // Enemy bullet
        this.enemybullets = game.add.group();
        this.enemybullets.enableBody = true;
        this.enemybullets.createMultiple(100, 'enemybullet');
        game.time.events.loop(1000, this.addEnemyBullet, this);
        // Super bullet
        this.superbullets = game.add.group();
        this.superbullets.enableBody = true;
        this.superbullets.createMultiple(2, 'superbullet');
        // keyboard input
        this.cursor = game.input.keyboard.createCursorKeys();
        // enable player
        game.physics.arcade.enable(this.player);
        game.physics.arcade.enable(this.player2);
        // Add vertical gravity to the player
        this.player.body.gravity.y = 0;
        this.player2.body.gravity.y = 0;
        // change Level
        game.global.level = 0;
        setTimeout(function(){
            game.global.level = 1;
        }, 20000);
        // score
        game.global.score = 0;
        // Display the score
        this.scoreLabel = game.add.text(30, 10, 'score: 0',
        { font: '18px Arial', fill: '#ffffff' });
        this.wasd = {
            up: game.input.keyboard.addKey(Phaser.Keyboard.W),
            left: game.input.keyboard.addKey(Phaser.Keyboard.A),
            right: game.input.keyboard.addKey(Phaser.Keyboard.D),
            down: game.input.keyboard.addKey(Phaser.Keyboard.S),
            space: game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR),
            ctrl: game.input.keyboard.addKey(Phaser.Keyboard.CONTROL)
        };
        // block
        this.trees = game.add.group();
        this.trees.enableBody = true;
        this.leftTree = game.add.sprite(-10, 0, 'tree', 0, this.trees);
        this.rightTree = game.add.sprite(600, 0, 'tree', 0, this.trees);
        this.topTree = game.add.sprite(0, 0, 'tree', 0, this.trees);
        this.bottomTree = game.add.sprite(0, game.height, 'tree', 0, this.trees);
        this.leftTree.scale.setTo(0.2,10);
        this.rightTree.scale.setTo(0.2,10);
        this.topTree.scale.setTo(10,0.1);
        this.bottomTree.scale.setTo(10,0.1);
        this.trees.setAll('body.immovable', true);
        this.trees.setAll('alpha', 0);
        // Create the emitter
        this.emitter = game.add.emitter(0, 0, 200);
        // Set the flame image for the particles
        this.emitter.makeParticles('flame');
        this.emitter.setYSpeed(-30, 30);
        this.emitter.setXSpeed(-30, 30);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 0;
        // buff
        this.aiming_icons = game.add.group();
        this.aiming_icons.enableBody = true;
        this.aiming_icons.createMultiple(1, 'aiming_icon');
        this.defend_icons = game.add.group();
        this.defend_icons.enableBody = true;
        this.defend_icons.createMultiple(1, 'defend_icon');
        this.powerbullet_icons = game.add.group();
        this.powerbullet_icons.enableBody = true;
        this.powerbullet_icons.createMultiple(1, 'powerbullet_icon');
        game.time.events.loop(8000, this.genBuff, this);
        // buff_defend
        game.global.save_circle = game.add.sprite(game.width/2,game.height/2, 'save_circle');
        game.global.save_circle.anchor.setTo(0.5, 0.5);
        game.global.save_circle.alpha = 0;
        game.global.p1SaveCircle = false;
        game.global.save_circle2 = game.add.sprite(game.width/2,game.height/2, 'save_circle');
        game.global.save_circle2.anchor.setTo(0.5, 0.5);
        game.global.save_circle2.alpha = 0;
        game.global.p2SaveCircle = false;
        // buff_aiming
        game.global.p1Aiming = false;
        game.global.p2Aiming = false;
        // buff_powerbullet
        game.global.powerbullet = game.add.sprite(0,0, 'powerbullet');
        game.physics.arcade.enable(game.global.powerbullet);
        game.global.powerbullet.anchor.setTo(0.5,1);
        game.global.powerbullet.alpha = 0;
        game.global.p1PowerBullet = false;
        game.global.p2PowerBullet = false;
        // boss
        setTimeout(function(){
            game.global.level = 2;
            game.global.boss = game.add.sprite(game.width/2 , -100, 'boss');
            game.global.boss.anchor.setTo(0.5, 1);
            game.physics.arcade.enable(game.global.boss);
            game.add.tween(game.global.boss).to({x:game.width/2,y:100}, 500).easing(Phaser.Easing.Bounce.Out).start();
        }, 40000);
        game.global.bosshealth = 150;
        game.global.bossPowerHurt = false;
        game.global.bossbullet = game.add.sprite(0,0, 'bossbullet');
        game.global.bossbullet.scale.setTo(1,1.5);
        game.global.bossbullet.anchor.setTo(0.5,0);
        game.physics.arcade.enable(game.global.bossbullet);
        game.global.bossbullet.alpha = 0;
        game.time.events.loop(5000, this.bossAttack, this);
        // warning
        this.warning_right = game.add.sprite(450 , game.height/2, 'warning');
        this.warning_right.anchor.setTo(0.5, 0.5);
        this.warning_right.alpha = 0;
        this.warning_left = game.add.sprite(150 , game.height/2, 'warning');
        this.warning_left.anchor.setTo(0.5, 0.5);
        this.warning_left.alpha = 0;
    },
    update: function() {
        this.storePower();
        this.movePlayer();
        this.useSuper();
        game.physics.arcade.overlap(game.global.powerbullet ,this.enemies
            , this.hitEnemy_power, null, this);
        game.physics.arcade.overlap(game.global.powerbullet ,this.enemybullets
            , this.clearbullet_power, null);

        game.physics.arcade.overlap(this.bullets,this.enemies
            , this.hitEnemy, null, this);
        game.physics.arcade.overlap(this.bullets2,this.enemies
            , this.hitEnemy, null, this);
        game.physics.arcade.overlap(this.superbullets,this.enemies
            , this.hitEnemy, null, this);
        game.physics.arcade.overlap(this.player,this.enemies
            , this.playerDie, null, this);
        game.physics.arcade.overlap(this.player,this.enemybullets
            , this.playerDie, null, this);
        game.physics.arcade.overlap(this.player2,this.enemies
            , this.playerDie_2, null, this);
        game.physics.arcade.overlap(this.player2,this.enemybullets
            , this.playerDie_2, null, this);
        game.physics.arcade.overlap(this.superbullets,this.enemybullets
            , this.clearbullet, null, this);
        game.physics.arcade.collide(this.player, this.trees);
        game.physics.arcade.collide(this.player2, this.trees);
        game.physics.arcade.collide(this.enemies, this.trees);
        this.clearEnemy();
        // buff
        game.physics.arcade.overlap(this.player,this.aiming_icons
            , this.p1_aiming, null, this);
        game.physics.arcade.overlap(this.player2,this.aiming_icons
            , this.p2_aiming, null, this);
        game.physics.arcade.overlap(this.player,this.defend_icons
            , this.p1_defend, null, this);
        game.physics.arcade.overlap(this.player2,this.defend_icons
            , this.p2_defend, null, this);
        game.physics.arcade.overlap(this.player,this.powerbullet_icons
            , this.p1_powerbullet, null, this);
        game.physics.arcade.overlap(this.player2,this.powerbullet_icons
            , this.p2_powerbullet, null, this);
        // update save_circle pos
        this.moveSaveCircle();
        this.usePowerBullet();
        // boss
        if(game.global.level==2){
            this.moveBossBullet();
            game.physics.arcade.overlap(this.player, game.global.bossbullet
                , this.playerHitByBoss, null, this);
            game.physics.arcade.overlap(this.player2, game.global.bossbullet
                , this.player2HitByBoss, null, this);
            game.physics.arcade.collide(this.player, game.global.boss
                , this.playerHitByBoss, null, this);
            game.physics.arcade.collide(this.player2, game.global.boss
                , this.player2HitByBoss, null, this);
            game.physics.arcade.overlap(game.global.powerbullet , game.global.boss
                , this.hitBoss_power, null, this);
            game.physics.arcade.overlap(this.bullets, game.global.boss
                , this.hitBoss, null, this);
            game.physics.arcade.overlap(this.bullets2, game.global.boss
                , this.hitBoss, null, this);
            game.physics.arcade.overlap(this.superbullets, game.global.boss
                , this.hitBoss, null, this);    
        }
    },
    hitBoss_power: function(){
        if(!game.global.bossPowerHurt){
            game.global.bossPowerHurt = true;
            game.global.bosshealth -= 5;
            this.emitter.x = game.global.boss.x;
            this.emitter.y = game.global.boss.y - 10;
            this.emitter.start(true, 800, null, 200);
            game.time.events.add(1000,
                function() {game.global.bossPowerHurt=false;}
                ,this);
            if(game.global.bosshealth<=0){
                game.global.score += 100;
                this.scoreLabel.text = 'score: ' + game.global.score;
                game.global.boss.kill();
                game.global.bossbullet.kill();
                game.camera.fade(0x000000, 2000);
                game.time.events.add(2000,
                    function() {game.state.start('over');}
                    ,this);
            }
        }
    },
    hitBoss: function(boss,bullet){
        bullet.kill();
        game.global.bosshealth--;
        this.emitter.x = bullet.x;
        this.emitter.y = bullet.y;
        this.emitter.start(true, 800, null, 50);
        if(game.global.bosshealth<=0){
            game.global.score += 100;
            this.scoreLabel.text = 'score: ' + game.global.score;
            boss.kill();
            game.global.bossbullet.kill();
            game.camera.fade(0x000000, 2000);
            game.time.events.add(2000,
                function() {game.state.start('over');}
                ,this);
        }
    },
    bossAttack: function(){
        if(game.global.level==2 && game.global.bossbullet){
            var turn = game.rnd.pick([-1, 1]);
            if(turn == 1){
                game.add.tween(this.warning_right).to({alpha: 1},900).yoyo(true).start();
            }
            else{
                game.add.tween(this.warning_left).to({alpha: 1},900).yoyo(true).start();
            }
            game.time.events.add(1000,
                function() {
                    game.add.tween(game.global.bossbullet).to({alpha: 1},500).start();
                    game.add.tween(game.global.boss).to({x: game.global.boss.x + turn*300 },1000).yoyo(true).start();
                    setTimeout(function(){
                        game.add.tween(game.global.bossbullet).to({alpha: 0},500).start();
                    },1500);
                },this);
        }
    },
    moveBossBullet: function(){
        game.global.bossbullet.x = game.global.boss.x;
        game.global.bossbullet.y = game.global.boss.y;
    },
    usePowerBullet: function(){
        if(game.global.p1PowerBullet && !game.global.playerDie){
            game.global.powerbullet.x = this.player.x;
            game.global.powerbullet.y = this.player.y;
            game.global.powerbullet.alpha = 1;
        }
        else if(game.global.p2PowerBullet && !game.global.player2Die){
            game.global.powerbullet.x = this.player2.x;
            game.global.powerbullet.y = this.player2.y;
            game.global.powerbullet.alpha = 1;
        }
        else{
            game.global.powerbullet.alpha = 0;
            game.global.p2PowerBullet = false;
            game.global.p1PowerBullet = false;
        }
    },
    moveSaveCircle: function(){
        game.global.save_circle.x = this.player.x;
        game.global.save_circle2.x = this.player2.x;
        game.global.save_circle.y = this.player.y + 20;
        game.global.save_circle2.y = this.player2.y + 20;
    },
    p1_aiming: function(player, item){
        item.kill();
        game.global.p1Aiming = true;
        setTimeout(function(){
            game.global.p1Aiming = false;
        },6000);
    },
    p2_aiming: function(player, item){
        item.kill();
        game.global.p2Aiming = true;
        setTimeout(function(){
            game.global.p2Aiming = false;
        },6000);
    },
    p1_defend: function(player, item){
        item.kill();
        game.global.p1SaveCircle = true;
        game.global.save_circle.alpha = 1;
        setTimeout(function(){
            game.global.p1SaveCircle = false;
            game.global.save_circle.alpha = 0;
        },5000);
    },
    p2_defend: function(player, item){
        item.kill();
        game.global.p2SaveCircle = true;
        game.global.save_circle2.alpha = 1;
        setTimeout(function(){
            game.global.p2SaveCircle = false;
            game.global.save_circle2.alpha = 0;
        },5000);
    },
    p1_powerbullet: function(player, item){
        item.kill();
        game.global.p1PowerBullet = true;
        setTimeout(function(){
            game.global.p1PowerBullet = false;
        },5000);
    },
    p2_powerbullet: function(player, item){
        item.kill();
        game.global.p2PowerBullet = true;
        setTimeout(function(){
            game.global.p2PowerBullet = false;
        },5000);
    },
    genBuff : function(){
        var buff = game.rnd.pick([0, 1, 2]);
        if(buff == 1){
            var aiming = this.aiming_icons.getFirstDead();
            if (!aiming) { return;}
            aiming.anchor.setTo(0.5, 0.5);
            aiming.reset(game.rnd.integerInRange(50,550), 0);
            aiming.body.gravity.y = 50;
            aiming.checkWorldBounds = true;
            aiming.outOfBoundsKill = true;
        }
        else if(buff == 2){
            var defend = this.defend_icons.getFirstDead();
            if (!defend) { return;}
            defend.anchor.setTo(0.5, 0.5);
            defend.reset(game.rnd.integerInRange(50,550), 0);
            defend.body.gravity.y = 50;
            defend.checkWorldBounds = true;
            defend.outOfBoundsKill = true;
        }
        else{
            var powerbullet = this.powerbullet_icons.getFirstDead();
            if (!powerbullet) { return;}
            powerbullet.anchor.setTo(0.5, 0.5);
            powerbullet.reset(game.rnd.integerInRange(50,550), 0);
            powerbullet.body.gravity.y = 50;
            powerbullet.checkWorldBounds = true;
            powerbullet.outOfBoundsKill = true;
        }
    },
    storePower : function() {
        if(game.global.powerEnough && game.global.powerEnough_2) return;
        if(game.global.powerEnough_2){
            game.global.powerState+=1;
            if(game.global.powerState % 60 == 0){
                var tmp = game.global.powerState/60 * 0.1;
                game.add.tween(this.power_icon).to({alpha: tmp},100).start();
                if(tmp==1){
                    game.global.powerEnough = true;
                    game.add.tween(this.power_icon).to({angle: 360},1000).start();
                }
            }
        }
        else if(game.global.powerEnough){
            game.global.powerState_2+=1;
            if(game.global.powerState_2 % 60 == 0){
                var tmp = game.global.powerState_2/60 * 0.1;
                game.add.tween(this.power_icon_2).to({alpha: tmp},100).start();
                if(tmp==1){
                    game.global.powerEnough_2 = true;
                    game.add.tween(this.power_icon_2).to({angle: 360},1000).start();
                }
            }
        }
        else{
            game.global.powerState+=1;
            if(game.global.powerState % 60 == 0){
                var tmp = game.global.powerState/60 * 0.1;
                game.add.tween(this.power_icon).to({alpha: tmp},100).start();
                if(tmp==1){
                    game.global.powerEnough = true;
                    game.add.tween(this.power_icon).to({angle: 360},1000).start();
                }
            }
            game.global.powerState_2+=1;
            if(game.global.powerState_2 % 60 == 0){
                var tmp2 = game.global.powerState_2/60 * 0.1;
                game.add.tween(this.power_icon_2).to({alpha: tmp2},100).start();
                if(tmp2==1){
                    game.global.powerEnough_2 = true;
                    game.add.tween(this.power_icon_2).to({angle: 360},1000).start();
                }
            }
        }
    },
    initClickUI : function(item) {
        item.inputEnable = true;
        item.events.onInputOver.add(this.over, this);
        item.events.onInputOut.add(this.out, this);
    },
    changeMute : function(item) {
        if(game.global.soundSize){
            game.global.soundSize = 0;
            this.sound_icon.visible = false;
            this.sound_mute.visible = true;
        }
        else{
            game.global.soundSize = 1;
            this.sound_icon.visible = true;
            this.sound_mute.visible = false;
        }
        game.global.bgm.volume = game.global.soundSize;
        this.shootSound.volume = game.global.soundSize;
    },
    changePause: function() {
        game.paused = !game.paused;
        if(this.resume.alpha == 0){
            this.pause.alpha = 0;
            this.resume.alpha = 1;
        }
        else{
            this.resume.alpha = 0;
            this.pause.alpha = 1;
        }

    },
    sound_plus: function(item) {
        game.add.tween(item.scale).to({x:1, y:1}, 100).start();
        if(game.global.soundSize >= 2) return;
        game.global.soundSize += 0.2;
        game.add.tween(this.sound_icon.scale).to({x:game.global.soundSize,y:game.global.soundSize},200)
        .yoyo(true).start();
        game.global.bgm.volume = game.global.soundSize;
        this.shootSound.volume = game.global.soundSize;
    },
    sound_minus: function(item) {
        game.add.tween(item.scale).to({x:1, y:1}, 100).start();
        if(game.global.soundSize <= 0) return;
        game.global.soundSize -= 0.2;
        game.add.tween(this.sound_icon.scale).to({x:game.global.soundSize,y:game.global.soundSize},200)
        .yoyo(true).start();
        game.global.bgm.volume = game.global.soundSize;
        this.shootSound.volume = game.global.soundSize;
    },
    over: function(item) {
        game.add.tween(item.scale).to({x:1.2,y:1.2}, 200).start();
    },
    out: function(item) {
        game.add.tween(item.scale).to({x:1, y:1}, 200).start();
    },
    clearEnemy: function(){
        this.enemies.forEachExists(function(enemy){
            if(enemy.y > 575)
                enemy.kill();
        }, this);
    },
    clearbullet_power: function(bullet, enemy){
        if(game.global.p2PowerBullet || game.global.p1PowerBullet)
            enemy.kill();
    },
    hitEnemy_power: function(bullet, enemy){
        if(game.global.p2PowerBullet || game.global.p1PowerBullet){
            this.emitter.x = enemy.x;
            this.emitter.y = enemy.y - 10;
            this.emitter.start(true, 800, null, 20);
            game.global.score += 5;
            this.scoreLabel.text = 'score: ' + game.global.score;
            enemy.kill();
        }
    },
    hitEnemy: function(bullet, enemy) {
        this.emitter.x = enemy.x;
        this.emitter.y = enemy.y - 10;
        this.emitter.start(true, 800, null, 20);
        game.global.score += 5;
        this.scoreLabel.text = 'score: ' + game.global.score;
        enemy.kill();
        bullet.kill();
    },
    clearbullet: function(superbullet, enemybullet) {
        enemybullet.kill();
    },
    useSuper: function() {
        if(this.wasd.space.isDown){
            var _bullet = this.superbullets.getFirstDead();
            if (!_bullet || !game.global.powerEnough || game.global.playerDie) { return;}
            _bullet.scale.setTo(0.5,0.5);
            _bullet.anchor.setTo(0.5,1);
            _bullet.reset(this.player.x, this.player.y +20);
            _bullet.body.velocity.y = -300;
            _bullet.checkWorldBounds = true;
            _bullet.outOfBoundsKill = true;
            game.global.powerEnough = false;
            game.global.powerState = 0;
            game.add.tween(this.power_icon).to({alpha: 0},200).start();
        }
        if(this.wasd.ctrl.isDown){
            var _bullet = this.superbullets.getFirstDead();
            if (!_bullet || !game.global.powerEnough_2 || game.global.player2Die) { return;}
            _bullet.scale.setTo(0.5,0.5);
            _bullet.anchor.setTo(0.5,1);
            _bullet.reset(this.player2.x, this.player2.y+20);
            _bullet.body.velocity.y = -300;
            _bullet.checkWorldBounds = true;
            _bullet.outOfBoundsKill = true;
            game.global.powerEnough_2 = false;
            game.global.powerState_2 = 0;
            game.add.tween(this.power_icon_2).to({alpha: 0},200).start();
        }
    },
    movePlayer: function() {
        if (this.cursor.left.isDown){
            this.player.body.velocity.x = -200;
            // Left animation
            this.player.animations.play('leftwalk'); 
        }
        else if(this.cursor.right.isDown){
            this.player.body.velocity.x = 200;
            // Right animation
            this.player.animations.play('rightwalk'); 
        }
        else{
            this.player.body.velocity.x = 0;
            this.player.animations.stop(); // Stop animations
            this.player.frame = 0; // Set frame (0 : stand)
        }
        if(this.wasd.left.isDown){
            this.player2.body.velocity.x = -200;
            // Left animation
            this.player2.animations.play('leftwalk2');
        }
        else if(this.wasd.right.isDown){
            this.player2.body.velocity.x = 200;
            // Left animation
            this.player2.animations.play('rightwalk2');
        }
        else{
            this.player2.body.velocity.x = 0;
            this.player2.animations.stop(); // Stop animations
            this.player2.frame = 0; // Set frame (0 : stand)
        }
        if (this.cursor.up.isDown && this.player.y > 10) {
            // this.player.body.velocity.y = -100;
            this.player.y -= 4; 
        }
        else if(this.cursor.down.isDown && this.player.y < 545){
            // this.player.body.velocity.y = 100;
            this.player.y += 4;
        }
        if(this.wasd.up.isDown && this.player2.y > 10){
            // this.player2.body.velocity.y = -100;
            this.player2.y -= 4;
        }
        else if(this.wasd.down.isDown && this.player2.y < 550){
            // this.player2.body.velocity.y = 100;
            this.player2.y += 4;
        }
    },
    addEnemy: function() {
        var enemy = this.enemies.getFirstDead();
        if (!enemy || game.global.level==2) { return;}
        enemy.anchor.setTo(0.5, 1);
        enemy.scale.setTo(0.8,0.8);
        enemy.reset(game.rnd.integerInRange(50,550), game.rnd.integerInRange(30,100));
        if(game.global.level==1){
            this.enemies.createMultiple(12, 'enemy');
            var _enemy = this.enemies.getFirstDead();
            _enemy.anchor.setTo(0.5, 1);
            _enemy.scale.setTo(0.8,0.8);
            _enemy.reset(game.rnd.integerInRange(50,550), game.rnd.integerInRange(30,100));
            _enemy.body.velocity.y = 20 * game.rnd.pick([0, 2, 4, 5]);
            _enemy.body.velocity.x = 100 * game.rnd.pick([-2, -1, 0, 1, 2]);
            _enemy.body.bounce.x = 1;
            _enemy.checkWorldBounds = true;
            _enemy.outOfBoundsKill = true;
            enemy.body.velocity.y = 20 * game.rnd.pick([0, 2, 4, 5]);
            enemy.body.velocity.x = 100 * game.rnd.pick([-2, -1, 0, 1, 2]);
        }
        else{
            enemy.body.velocity.x = 100 * game.rnd.pick([-1, 1]);
        }
        enemy.body.bounce.x = 1;
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
    },
    addEnemyBullet: function() {
        this.enemies.forEachExists(function(enemy){
            var _enemybullet = this.enemybullets.getFirstDead();
            if(!_enemybullet) { return; }
            _enemybullet.anchor.setTo(0.5,0.5);
            _enemybullet.reset(enemy.x, enemy.y);
            _enemybullet.body.velocity.y = 120;
            _enemybullet.checkWorldBounds = true;
            _enemybullet.outOfBoundsKill = true;
        }, this);
    },
    addBullet: function() {
        var _bullet = this.bullets.getFirstDead();
        if (!_bullet || game.global.playerDie || game.global.p1PowerBullet) { return;}
        this.shootSound.play();
        _bullet.anchor.setTo(0.5, 0.5);
        _bullet.reset(this.player.x, this.player.y);
        _bullet.body.velocity.y = -200;
        _bullet.checkWorldBounds = true;
        _bullet.outOfBoundsKill = true;
        if(game.global.p1Aiming){
            if(game.global.level==2){
                game.add.tween(_bullet).to({x:game.global.boss.x,y:game.global.boss.y},1500).start();
                return;
            }
            var enemy = this.enemies.getFirstExists();
            if(enemy){
                var vX = enemy.body.velocity.x;
                var vY = enemy.body.velocity.y;
                var distanceX = Math.abs(enemy.x - _bullet.x);
                var distanceY = Math.abs(enemy.y - _bullet.y);
                var distance = Math.sqrt(Math.pow(distanceX,2)+Math.pow(distanceY,2));
                var time = distance*5;
                var destX = enemy.x + vX*time/1000;
                var destY = enemy.y + vY*time/1000;
                if(destX < 0){
                    destX = 0 - destX;
                }
                else if(destX > 600){
                    destX = 1200 - destX;
                }
                game.add.tween(_bullet).to({x: destX, y:destY},time).start();
            }
        }
            
    },
    addBullet2: function() {
        var _bullet = this.bullets2.getFirstDead();
        if (!_bullet || game.global.player2Die || game.global.p2PowerBullet) { return;}
        this.shootSound.play();
        _bullet.anchor.setTo(0.5, 0.5);
        _bullet.reset(this.player2.x, this.player2.y);
        _bullet.body.velocity.y = -200;
        _bullet.checkWorldBounds = true;
        _bullet.outOfBoundsKill = true;
        if(game.global.p2Aiming){
            if(game.global.level==2){
                game.add.tween(_bullet).to({x:game.global.boss.x,y:game.global.boss.y},1500).start();
                return;
            }
            var enemy = this.enemies.getFirstExists();
            if(enemy){
                var vX = enemy.body.velocity.x;
                var vY = enemy.body.velocity.y;
                var distanceX = Math.abs(enemy.x - _bullet.x);
                var distanceY = Math.abs(enemy.y - _bullet.y);
                var distance = Math.sqrt(Math.pow(distanceX,2)+Math.pow(distanceY,2));
                var time = distance*5;
                var destX = enemy.x + vX*time/1000;
                var destY = enemy.y + vY*time/1000;
                if(destX < 0){
                    destX = 0 - destX;
                }
                else if(destX > 600){
                    destX = 1200 - destX;
                }
                game.add.tween(_bullet).to({x: destX, y:destY},time).start();
            }

        }
    },
    playerDie: function(player,enemy) {
        if(game.global.p1SaveCircle || game.global.playerHurt){
            enemy.kill();
            return;
        }
        else{
            game.global.playerHurt = true;
            setTimeout(function(){
                game.global.playerHurt = false;
            },600);
            game.add.tween(this.player).to({alpha: 0.5},500).yoyo(true).start();
            this.emitter.x = this.player.x;
            this.emitter.y = this.player.y + 10;
            this.emitter.start(true, 800, null, 15);
            enemy.kill();
            game.global.health--;
            game.camera.flash(0xffffff, 300);
            game.camera.shake(0.02, 300);
            if(game.global.health){
                if(game.global.health>1){
                    game.add.tween(this.blood_3).to({alpha: 0},500).start();
                }
                else{
                    game.add.tween(this.blood_2).to({alpha: 0},500).start();
                }
            }
            else{
                this.player.kill(); // Make player disappear.
                game.global.playerDie = true;
                this.power_icon.alpha = 0;
                game.add.tween(this.blood_1).to({alpha: 0},500).start();
                // Set delay
                if(!game.global.health_2){
                    game.time.events.add(1000,
                        function() {game.state.start('over');}
                        ,this);
                }
            }
        }
    },
    playerDie_2: function(player,enemy) {
        if(game.global.p2SaveCircle || game.global.player2Hurt){
            enemy.kill();
            return;
        }
        else{
            game.global.player2Hurt = true;
            setTimeout(function(){
                game.global.player2Hurt = false;
            },600);
            game.add.tween(this.player2).to({alpha: 0.5},500).yoyo(true).start();
            this.emitter.x = this.player2.x;
            this.emitter.y = this.player2.y + 10;
            this.emitter.start(true, 800, null, 15);
            enemy.kill();
            game.global.health_2--;
            game.camera.flash(0xffffff, 300);
            game.camera.shake(0.02, 300);
            if(game.global.health_2){
                if(game.global.health_2>1){
                    game.add.tween(this.blood_3_2).to({alpha: 0},500).start();
                }
                else{
                    game.add.tween(this.blood_2_2).to({alpha: 0},500).start();
                }
            }
            else{
                this.player2.kill(); // Make player disappear.
                game.global.player2Die = true;
                this.power_icon_2.alpha = 0;
                game.add.tween(this.blood_1_2).to({alpha: 0},500).start();
                // Set delay
                if(!game.global.health){
                    game.time.events.add(1000,
                        function() {game.state.start('over');}
                        ,this);
                }
            }
        }
    },
    playerHitByBoss: function(){
        if(game.global.p1SaveCircle || game.global.playerHurt || game.global.bossbullet.alpha<0.5)
            return;
        else{
            game.global.playerHurt = true;
            setTimeout(function(){
                game.global.playerHurt = false;
            },800);
            game.add.tween(this.player).to({alpha: 0.5},500).yoyo(true).start();
            this.emitter.x = this.player.x;
            this.emitter.y = this.player.y + 10;
            this.emitter.start(true, 800, null, 15);
            game.global.health--;
            game.camera.flash(0xffffff, 300);
            game.camera.shake(0.02, 300);
            if(game.global.health){
                if(game.global.health>1){
                    game.add.tween(this.blood_3).to({alpha: 0},500).start();
                }
                else{
                    game.add.tween(this.blood_2).to({alpha: 0},500).start();
                }
            }
            else{
                this.player.kill(); // Make player disappear.
                game.global.playerDie = true;
                this.power_icon.alpha = 0;
                game.add.tween(this.blood_1).to({alpha: 0},500).start();
                // Set delay
                if(!game.global.health_2){
                    game.time.events.add(1000,
                        function() {game.state.start('over');}
                        ,this);
                }
            }
        }
    },
    player2HitByBoss: function(){
        if(game.global.p2SaveCircle || game.global.player2Hurt || game.global.bossbullet.alpha < 0.5)
            return;
        else{
            game.global.player2Hurt = true;
            setTimeout(function(){
                game.global.player2Hurt = false;
            },800);
            game.add.tween(this.player2).to({alpha: 0.5},500).yoyo(true).start();
            this.emitter.x = this.player2.x;
            this.emitter.y = this.player2.y + 10;
            this.emitter.start(true, 800, null, 15);
            game.global.health_2--;
            game.camera.flash(0xffffff, 300);
            game.camera.shake(0.02, 300);
            if(game.global.health_2){
                if(game.global.health_2>1){
                    game.add.tween(this.blood_3_2).to({alpha: 0},500).start();
                }
                else{
                    game.add.tween(this.blood_2_2).to({alpha: 0},500).start();
                }
            }
            else{
                this.player2.kill(); // Make player disappear.
                game.global.player2Die = true;
                this.power_icon_2.alpha = 0;
                game.add.tween(this.blood_1_2).to({alpha: 0},500).start();
                // Set delay
                if(!game.global.health){
                    game.time.events.add(1000,
                        function() {game.state.start('over');}
                        ,this);
                }
            }
        }
    }
};