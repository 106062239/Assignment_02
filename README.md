# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description
* 連線版的code沒有傳到gitlab，有傳到ftp的空間裡。
* 因為gitlab不能用連線版，所以點play online會切換到Heroku的網址。
* play是玩單機雙人，左下的血滴、火球圖示是player1的，右邊的是player2的。player1用上下左右來移動，空白鍵放大招，player2用wasd來移動，左ctrl放大招。
* 由於要push到Heroku的話，檔案要放在根目錄才讀的到，這樣和firebase相衝(放在public)，所以連線版的結尾沒有firebase的leaderboard，只有顯示這次的分數。連線版和單機版的遊玩功能差不多，只是UI有點變動，玩家的圖片也都變成一樣的飛機。如果其中一個玩家按暫停，雙方玩家都會暫停，一方按繼續，兩邊都會繼續。連線版的移動可以用上下左右或wasd，大招是空白鍵。結束畫面quit是跳回gitlab的網頁，play again是重load Heroku的網頁，不然直接跳到play的state會有詭異的bug。連線是預設兩個人玩，所以第一個人進去後會先停滯不能動作，等到第二個人進來才開始遊戲。
* 有時候切換網址會被擋掉或者沒反應，所以在此放上網址，如果被擋掉可以直接打網址過去。
gitlab: https://106062239.gitlab.io/Assignment_02。
Heroku: https://assignment02-106062239.herokuapp.com。
* 重要提醒!!! : 連線版一定要開始遊戲之後才能按F5重新整理(如果有需要的話)，最好是玩到結束，不然可能server會有錯。

# Basic Components Description : 
1. Jucify mechanisms : 
    * Level: 一開始敵機只會左右移動，20秒後敵機數量會增加，且會以不同速度往前衝(隨機的)，40秒後會有boss，且停止產生敵機。
    * Skill: 發射衝擊波，會清除碰到的敵機子彈。畫面上的火球圖示再使用大招後，會透明，再慢慢不透明(代表冷卻時間)，冷卻好了火球會轉一圈。

2. Animations : [玩家有左右移動的動畫]
3. Particle Systems : [子彈打到玩家或敵機時，會有簡單的爆炸粒子效果]
4. Sound effects : [有背景音樂和子彈發射時的雷射光音效]
5. Leaderboard : [單機版遊戲結束時會有realtime的排行榜，顯示分數最高的前五名]
6. UI: 
    * 血滴代表所剩的生命
    * 火球圖示代表大招
    * 右上角的聲音按鈕按下去可以靜音，再按會回復初始音量，右邊的加、減可以調整音量大小，點了聲音圖示會變大或變小且回復原本大小(所以不能按太快，不然會變不回原樣)。

# Bonus Functions Description : 
1. [Multi-player game] : [有On-line和Off-line，細節放在上面Website Detail Description]
2. [Enhanced items] :
    * 靠重力加速度掉落，所以會越掉越快。
    * 每8秒會隨機掉下一個道具圖示，長的像火箭的是自動瞄準，長的像盾牌的是防護罩，長的像三個子彈的是雷射炮，吃到道具的玩家一定時間內擁有額外效果。
    * 自動瞄準 : 子彈會判斷與敵機的距離和敵機移動的速度，飛到特定的位置。
    * 防護罩 : 一定時間內無敵。
    * 雷射炮 : 子彈改成一條雷射炮，可以清除敵機的子彈。
3. [Boss] : [每隔幾秒會隨機往左或往右移動且噴射雷射炮，在boss攻擊前會有警告圖示顯示在左邊或右邊，代表boss要移動的方向，擊敗boss後會有fade out效果。]
